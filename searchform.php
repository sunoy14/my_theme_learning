<form role="search" id="searchform" method="get" action="<?php echo home_url('/')?>">
	<label class="searchform" for="searchform_text">Search</label>
	<input type="text" id="s" name="s" value="" placeholder="<?php echo the_search_query(); ?>" />
	<input type="submit" id="searchform_submit" name="searchform_submit" value="Search" />
</form>