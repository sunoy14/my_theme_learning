<!-- secondary_column -->
<div class="secondary_col clearfix">
	<?php dynamic_sidebar('sidebar1'); ?>
	<?php
		$args = array(
			'post_type' => 'products',
			'tax_query' => array(
				array(
					'taxonomy' => 'type',
					'field' => 'slug',
					'terms' => 'test product'
				)
		));
		$products = new WP_Query($args);
		if($products->have_posts()):
			while($products->have_posts()):
				$products->the_post();
				the_title();
				$product_metadata = get_post_custom(116);
				foreach($product_metadata as $name=>$value){
					echo '<br /><br /><strong>' . $name . '</strong> => ';
					foreach($value as $nameAr=>$valueAr){
						echo '<br />' . $nameAr . "=>";
						echo var_dump($valueAr);
					}
				}
			endwhile;
		else:
			echo 'No posts found';
		endif;
		wp_reset_postdata();
		_e('howdy neighbour!', 'prowp_plugin');
	?>
</div><!-- /secondary_col -->