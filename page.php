<?php
	get_header();
	if(have_posts()){
		while(have_posts()){
			the_post();			
				if(has_children() || $post->post_parent>0){
?>
					<!-- children page menus -->
					<nav class="child_pages_menu clearfix">
						<span class="parent_link"><a href="<?php echo get_the_permalink(get_top_ancestor_id()); ?>"><?php echo get_the_title(get_top_ancestor_id()); ?></a></span>
						<ul>
<?php
						$args = array(
							'child_of' => get_top_ancestor_id(),
							'title_li' => ''
						);
						wp_list_pages($args);
?>
						</ul>
					</nav><!-- /child_pages_menu -->
<?php
				}
?>			
			<!-- page content wrapper -->
			<div class="page_content_wrapper">
				<!-- main content -->
				<div class="main_content">
					<p><?php the_content(); ?></p>
				</div><!-- /main_content -->
				
				<!-- sidebar -->
				<!--<div class="sidebar">
				
				</div> -->
			</div><!-- /page_content -->
<?php
		}
	}
	else{
		echo '<p>No content found</p>';
	}
	get_footer();
?>