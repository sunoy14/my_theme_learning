<?php
	get_header();
?>
		<!-- main column -->
		<!-- page content wrapper -->
		<div class="page_content_wrapper">
			<div class="main_col">
<?php 
				if(have_posts()){						
					while(have_posts()){
						the_post();
?>
						<article class="blog_post">
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<p class="post_info"><?php the_time('F jS, Y g:i a'); ?> | by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> | Posted in
								<?php 
									$categories = get_the_category();
									$seperator = ',';
									$output = '';
									if($categories){
										foreach($categories as $category){
											$output .= '<a href="' . get_category_link('term_id') . '">' . $category->cat_name . '</a>' . $seperator;
										}
										echo trim($output, $seperator);
									}
								?>
							</p>
		<?php
							the_content();
							echo '<div style="clear:both;"></div>';
							echo '<br />';
							edit_post_link();
							echo '<div style="clear:both;"></div>';
							echo '<br />';
							echo '<hr />';
							comment_form();
		?>
						</article>
		<?php
					}
		?>
<?php						
				}
				else{
					echo '<p>No content found</p>';
				}
?>
			</div><!-- /main_col -->
<?php
			get_sidebar();
?> 
		</div><!-- /page content wrapper -->
<?php
	get_footer();
?>

<?php 
	$aa = get_option('prowp_display_mode');
	foreach($aa as $a){
		echo $a['prowp_display_mode'] . "<br />";
	}
?>