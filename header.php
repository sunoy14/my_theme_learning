<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div class="container">
		<!-- site header -->
		<div class="site_header clearfix">
			<!-- site name or logo -->
			<h1 class="blog_name"><a href="<?php home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
			<h5 class="blog_description"><?php bloginfo('description'); ?></h5>
		
			<!-- site navigation menus -->
			<nav class="site_nav">
				<?php 
					$args = array('theme_location' => 'primary');
					wp_nav_menu($args); 
				?>
			</nav><!-- /site_nav -->
			
			<!-- search -->
			<div class="site_search">
				<?php get_search_form(); ?>
			</div><!-- /site_search -->
		</div><!-- /site_header -->
		
		<!-- social media -->
		<div class="social_media">
			<p>social media</p>
			<p>social media</p>
			<p>social media</p>
			<p>social media</p>
		</div><!-- /social media -->