		<!-- footer -->
		<div class="site_footer clearfix">
			<div class="footer_sidebars_container">
				<?php 
					if(is_active_sidebar('footer_widget1')){
				?>
						<div class="footer_widget">	
							<?php dynamic_sidebar('footer_widget1'); ?>
						</div>
				<?php
					}
						
					if(is_active_sidebar('footer_widget2')){
				?>
						<div class="footer_widget">	
							<?php dynamic_sidebar('footer_widget2'); ?>
						</div>
				<?php
					}
						
					if(is_active_sidebar('footer_widget3')){
				?>
						<div class="footer_widget">	
							<?php dynamic_sidebar('footer_widget3'); ?>
						</div>
				<?php
					}
						
					if(is_active_sidebar('footer_widget4')){
				?>
						<div class="footer_widget">	
							<?php dynamic_sidebar('footer_widget4'); ?>
						</div>
				<?php
					}
				?>
			</div><!-- footer_sidebars_container -->
		</div><!-- /footer -->
		
		<!-- footer nav menu -->
			<nav class="footer_nav_menu clearfix">
				<?php
					//footer menu
					$args = array(
						'theme_location' => 'footer'
					);
					wp_nav_menu($args); 
				?>
			</nav><!-- /footer_nav_menu -->
	</div><!-- /container -->
	<p class="copyright">Copyright &copy; 2016 | All rights reserved</p>
	<p class="credits"><a href="http://www.arunwebsolus.com">Developed by Arunwebsolus</a></p>
	<?php wp_footer(); ?>
</body>
</html>