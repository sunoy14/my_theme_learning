<?php
	/*
	Theme Name: my_theme_learning
	Theme URI: http://www.arunwebsolus.com 
	Description: This is a test theme that I use for leanring wordpress. It may not function as expected and contains codes
	that you don't want.
	Version: 1.0
	Author Name: Millo Lailang
	Author URI: http://www.arunwebsolus.com 
	Author Email: sunoy14socmed@gmail.com 
	License: GPLv2
	*/
	
	get_header();
?>
	<!-- page content wrapper -->
	<div class="page_content_wrapper">
		<!-- main column -->
		<div class="main_col">
<?php 
			if(have_posts()){						
				while(have_posts()){
					the_post();
					get_template_part('content', get_post_format());
				}						
			}
?>
		</div><!-- /main_col -->
<?php
		get_sidebar();
?> 
	</div><!-- /page content wrapper -->
<?php
	get_footer();
?>