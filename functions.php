<?php 	
	//register the styles
	function register_my_styles(){
		wp_register_style('my_stylesheet', get_stylesheet_uri(), array(), false, 'screen');
	}
	add_action('init', 'register_my_styles');
	
	//enqueue the styles
	function enqueue_styles(){
		wp_enqueue_style('my_stylesheet');
	}
	add_action('wp_enqueue_scripts', 'enqueue_styles');
	
	//theme setup
	function theme_setup(){
		//register nav menus
		register_nav_menus(array(
			'primary' => __('Primary Menu'),
			'footer' => __('Footer Menu')
		));
		
		add_theme_support('post-thumbnails');
		
		add_theme_support('post-formats', array('aside', 'gallery', 'link'));
	}
	add_action('after_setup_theme', 'theme_setup');
	
	//get top ancestor
	function get_top_ancestor_id(){
		global $post;
		if($post->post_parent){
			$ancestors = array_reverse(get_post_ancestors($post->Id));
			return $ancestors[0];
		}
		return $post->ID;
	}
	
	//does have children
	function has_children(){
		global $post;
		$pages = get_pages('child_of=' . $post->ID);
		return count($pages);
	}
	
	// sidebar register 
	function sidebar_register(){
		register_sidebar(array(
			'name' => 'Primary Sidebar',
			'id' => 'sidebar1',
			'before_widget' => '<div class="widget_item">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'Home_top_widget',
			'id' => 'home_top_widget',
			'before_widget' => '<div class="home_top_widget">',
			'after_widget' => '</div>'
		));
		
		register_sidebar(array(
			'name' => 'home_middle_widget 1',
			'id' => 'home_middle_widget1',
			'before_widget' => '<div class="home_middle_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'home_middle_widget 2',
			'id' => 'home_middle_widget2',
			'before_widget' => '<div class="home_middle_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'home_middle_widget 3',
			'id' => 'home_middle_widget3',
			'before_widget' => '<div class="home_middle_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'home_bottom_widget',
			'id' => 'home_bottom_widget',
			'before_widget' => '<div class="home_bottom_widget">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'Footer widget 1',
			'id' =>'footer_widget1',
			'before_widget' => '<div class="footer_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'Footer widget 2',
			'id' =>'footer_widget2',
			'before_widget' => '<div class="footer_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'Footer widget 3',
			'id' =>'footer_widget3',
			'before_widget' => '<div class="footer_widget_items">',
			'after_widget' => '</div>'
		));
		register_sidebar(array(
			'name' => 'Footer widget 4',
			'id' =>'footer_widget4',
			'before_widget' => '<div class="footer_widget_items">',
			'after_widget' => '</div>'
		));
	}
	add_action('widgets_init', 'sidebar_register');
	
	function prowp_register_my_post_types(){
		register_post_type('products', array(
			'labels' => array(
				'name' => 'Products',
				'add_new'=>'Add New',
			),
			'public' => true,
			'supports' => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'comments',
				'trackbacks',
				'custom-fields',
				'page-attributes',
				'revisions',
				'post-formats'
			),
			'hierarchical'=>true,
			
		));
	}
	add_action('init', 'prowp_register_my_post_types');
	
	//custom taxonomy register
	function prowp_define_product_type_taxonomy(){
		$labels = array(
			'name' => 'Type',
			'singular_name' => 'Types',
			'search_items' => 'Search Types',
			'all_items' => 'All Types',
			'parent_item' => 'Parent Type',
			'parent_item_colon' => 'Parent Type:',
			'edit_item' => 'Edit Type',
			'update_item' => 'Update Type',
			'add_new_item' => 'Add New Type',
			'new_item_name' => 'New Type Name',
			'menu_name' => 'Type',
			'view_item' => 'View Types'
		);
		
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'query_var' => true,
			'rewrite' => true
		);
		
		register_taxonomy('type', 'products', $args);
	}
	add_action('init', 'prowp_define_product_type_taxonomy');
	
	function prowp_install(){
		global $wp_version;
		if(version_compare($wp_version, '4.3', '<')){
			wp_die('You need to update your wordpress version');
		}
	}
	register_activation_hook(__FILE__, 'prowp_install');
	
	function test($content){
		if(is_single()){
			$content .= 'Subscribe to my <a href="#">rss feed</a>';
		}
		return $content;
	}
	add_filter('the_content', 'test');
	
	//adding test options to database
	$prowp_options_arr = array(
		'prowp_display_mode' => 'spooky',
		'prowp_default_movie' => 'Halloween',
		'prowp_default_book' => 'Professional Wordpress'
	);
	update_option('prowp_plugin_options', $prowp_options_arr); //can also use add_option() instead of update_option(). delete_option('option_name') is used to delete an option

	//add top level plugin menu
	function prowp_create_menu(){
		add_menu_page('Halloween Plugin Page', 'Halloween Plugin', 'manage_options', 'prowp_main_menu', 'prowp_settings_page', plugins_url('/images/wordpress.png', __FILE__), 66);
		add_submenu_page('prowp_main_menu', 'Halloween Settings Page', 'Settitngs', 'manage_options', 'halloween_settings', 'prowp_settings_page');
		add_submenu_page('prowp_main_menu', 'Halloween Support Page', 'Support', 'manage_options', 'halloween_support', 'prowp_support_page');
		
		//call register settings function
		add_action('admin_init', 'prowp_register_settings');
	}
	
	add_action('admin_menu', 'prowp_create_menu');
	
	function prowp_register_settings(){
			//register settings
			register_setting('prowp_settings_group', 'prowp_options', 'prowp_sanitize_settings');
	}
	
	function prowp_sanitize_options($input){
		$input['option_name'] = sanitize_text_field($input['option_name']);
		$input['option_email'] = sanitize_email($input['option_email']);
		$input['option_url'] = esc_url($input['option_url']);
		return $input;
	}
	
	function prowp_settings_page(){
?>
		<div class = "wrap">
			<h2>Halloween Plugin Options</h2>
			<form method="post" action="options.php">
				<?php 
					settings_fields('prowp_settings_group');
					$prowp_options = get_option('prowp_options');
				?>
				<table class="form_table">
					<tr valign="top">
					<th scope="row">Name</th>
					<td><input type="text" name="prowp_options[option_name]" value="<?php echo esc_attr($prowp_options['option_name']); ?>" /></td>
					</tr>
					<tr valign="top">
					<th scope="row">Email</th>
					<td><input type="text" name="prowp_options[option_email]" value="<?php echo esc_attr($prowp_options['option_email']); ?>" /></td>
					</tr>
					<tr valign="top">
					<th scope="row">URL</th>
					<td><input type="text" name="prowp_options[option_url]" value="<?php echo esc_url($prowp_options['option_url']); ?>" /></td>
					</tr>
				</table>
				<p class="submit">
					<input type="submit" class="button_primary" value="Save Changes" />
				</p>
			</form>
		</div>
<?php 
	}
	
	function prowp_settings_init(){
		//create the new settings section on the settings > reading page-attributes
		add_settings_section('prowp_setting_section', 'Halloween Plugin Settings', 'prowp_setting_section', 'reading');
		
		add_settings_field('prowp_setting_enable_id', 'Enable Halloween Feature?', 'prowp_setting_enabled', 'reading', 'prowp_setting_section');
		
		add_settings_field('prowp_saved_setting_name_id', 'Your Name', 'prowp_setting_name', 'reading', 'prowp_setting_section');
		
		register_setting('reading', 'prowp_setting_values', 'prowp_sanitize_settings');
	}
	
	//execute our settings section function
	add_action('admin_init', 'prowp_settings_init');
	
	function prowp_sanitize_settings($input){
		$input['enabled'] = ($input['enabled'] == 'on') ? 'on' : '';
		$input['name'] = sanitize_text_field($input['name']);
		return $input;
	}
	
	//settings section
	function prowp_setting_section(){
		echo '<p>Configure the Halloween plugin options below</p>';
	}
	
	//Create the enabled checkbox option to save the checkbox value
	function prowp_setting_enabled(){
		//load plugin options
		$prowp_options = get_option('prowp_setting_values');
		//display the checkbox form field
		echo '<input type="checkbox"' . checked($prowp_options['enabled'], 'on', false) . 'name="prowp_setting_values[enabled]" />';
	}
	
	//Create the text field setting to save the name
	function prowp_setting_name(){
		//load the option value
		$prowp_options = get_option('prowp_setting_values');
		//display the text form field
		echo '<input type="text" name="prowp_setting_values[name]" value="' . esc_attr($prowp_options['name']) . '" />';
	}
	
	//custom meta box
	add_action('add_meta_boxes', 'prowp_meta_box_init');
	
	function prowp_meta_box_init(){
		add_meta_box('prowp_meta', 'Product Information', 'prowp_meta_box', 'post', 'side', 'default');
	}
	
	function prowp_meta_box($post, $box){
		$prowp_featured = get_post_meta($post->ID, '_prowp_type', true);
		$prowp_price = get_post_meta($post->ID, '_prowp_price', true);
		wp_nonce_field(plugin_basename(__FILE__), 'prowp_save_meta_box');
		//custom metabox form elements
		echo '<p>
				Price: <input type="text" name="prowp_price" value="' . esc_attr($prowp_price) . '" size="5" />
			</p>';
		echo '<p>Type: 
				<select name="prowp_product_type" id="prowp_product_type">
					<option value="normal"' . selected($prowp_featured, 'normal', false) . '>Normal</option>
					<option value="special"' . selected($prowp_featured, 'special', false) . '>Special</option>
					<option value="featured"' . selected($prowp_featured, 'featured', false) . '>Featured</option>
					<option value="clearance"' . selected($prowp_featured, 'clearance', false) . '>Clearance</option>
				</select>
			</p>';
	}
	
	//hook to save our meta box data when the post is saved
	add_action('save_post', 'prowp_save_meta_box');
	function prowp_save_meta_box($post_id){
		if(isset($_POST['prowp_product_type'])){
			//if auto saving, skip saving our meta box data
			if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
				return;
			}
			wp_verify_nonce(plugin_basename(__FILE__), 'prowp_save_meta_box');
			//save the meta box data as post meta using the post ID as a unique prefix
			update_post_meta($post_id, '_prowp_type', sanitize_text_field($_POST['prowp_product_type']));
			update_post_meta($post_id, '_prowp_price', sanitize_text_field($_POST['prowp_price']));
		}
	}
	
	//add plugin settings under settings menu
	function prowp_create_settings_submenu(){
		add_options_page('Halloween Settings Page', 'Halloween Settings', 'manage_options', 'halloween_settings_menu', 'prowp_settings_page');
	}
	add_action('admin_menu', 'prowp_create_settings_submenu');
	
	//shortcode
	add_shortcode('mytwitter', 'prowp_twitter');
	
	function prowp_twitter($atts, $content = null){
		extract(shortcode_atts(array('person' => 'brad', 'color' =>'green'), $atts));
	/* The original that was written in the book....
		if($person == 'brad'){
		return '<a href="http://www.twitter.com/williamsba">@williamsba</a>';
		}
		elseif($person == 'david'){
			return '<a href="http://www.twitter.com/mirmillo">@mirmillo</a>';
		}
	*/
		return '<p style="color:' . $color . ';">@' . $person . '</p>';
	}
	
		//widgets
	add_action('widgets_init', 'prowp_register_widgets');
	
	function prowp_register_widgets(){
		register_widget('prowp_widget');
	}
	
	class prowp_widget extends WP_Widget{
		//process our new widget
		function __construct(){
			$widget_ops = array(
				'classname' => 'prowp_widget_class',
				'description' => 'Example widget that displays a user\'s bio.'
			);
			parent::__construct('prowp_widget', 'Bio Widget', $widget_ops);
		}
		
		//build our widget settings form 
		function form($instance){
			$defaults = array(
				'title' => 'My Bio',
				'name' => 'Michael Myers',
				'bio' => ''
			);
			$instance = wp_parse_args((array)$instance, $defaults);
			$title = $instance['title'];
			$name = $instance['name'];
			$bio = $instance['bio'];
?>
			<p>Title: <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
			<p>Name: <input class="widefat" name="<?php echo $this->get_field_name('name'); ?>" type="text" value="<?php echo esc_attr($name); ?>" /></p>
			<p>Bio: <textarea class="widefat" name="<?php echo $this->get_field_name('bio'); ?>"><?php echo esc_textarea($bio); ?></textarea>
<?php 
			
		}
		
		//save our widget settings
		function update($new_instance, $old_instance){
			$instance = $old_instance;
			$instance['title'] = sanitize_text_field($new_instance['title']);
			$instance['name'] = sanitize_text_field($new_instance['name']);
			$instance['bio'] = sanitize_text_field($new_instance['bio']);
			return $instance;
		}
		
		//display our widget
		function widget($args, $instance){
			extract($args);
			echo $before_widget;
			$title = apply_filters('widget_title', $instance['title']);
			$name = (empty($instance['name']))?' ':$instance['name'];
			$bio = (empty($instance['bio']))?'nbsp;':$instance['bio'];
			if(!empty($title)){
				echo $before_title . '<b>' . esc_html($title) . '</b>' . $after_title;
			}
			echo '<p><b>Name:</b> ' . esc_html($name) . '</p>';
			echo '<p><b>Bio:</b> ' . esc_html($bio) . '</p>';
			
			echo $after_widget;
			
		}
	}
?>