<?php
	get_header();
?>
	<!-- page content wrapper -->		
	<div class="page_content_wrapper clearfix">		
		<!-- main column -->
		<div class="main_col">
			<!-- home top box -->
			<div class="home_top_box">
<?php 
				dynamic_sidebar('home_top_widget');
?>
			</div><!-- /home top box -->
			
			<!-- home middle boxes -->
			<div class="home_middle_boxes clearfix">
				<!-- one-half -->
				<div class="one-half">
<?php
					dynamic_sidebar('home_middle_widget1');
?>
				</div><!-- /one-half -->
				
				<!-- one-half news-->
				<div class="one-half last">			
<?php
					dynamic_sidebar('home_middle_widget2');
?>
				</div><!-- /one-half -->
				
				<!-- one-half news-->
				<div class="one-half last">			
<?php
					dynamic_sidebar('home_middle_widget3');
?>
				</div><!-- /one-half -->
				
			</div><!-- /home middle boxes -->

			<!-- home bottom box -->
			<div class="home_bottom_box">
				<?php
				if(is_active_sidebar('home_bottom_widget')){
					dynamic_sidebar('home_bottom_widget');
				}
				else{
					if(have_posts()){						
						while(have_posts()){
							the_post();
?>
							<h2>
<?php 
								the_title();
?>
							</h2>
<?php 
							the_content();
						}						
					}
				}
?>
			</div><!-- /home bottom box -->
			
		</div><!-- /main_col -->
<?php
		get_sidebar();
?> 
	</div><!-- /page content wrapper -->
<?php
	get_footer();
?>