<?php
	get_header();
?>
	<!-- page content wrapper -->
	<div class="page_content_wrapper">
		<!-- main column -->
		<div class="main_col">
<?php 
			echo '<b>Search results for</b> \'';
			the_search_query();
			echo '\'';
			if(have_posts()){						
				while(have_posts()){
					the_post();
					get_template_part('content', get_post_format());
				}						
			}
			else{
				echo '<p>No content found</p>';
			}
?>
		</div><!-- /main_col -->
<?php
		get_sidebar();
?> 
	</div><!-- /page content wrapper -->
<?php
	get_footer();
?>