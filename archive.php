<?php
	get_header();
?>
	<!-- page content wrapper -->
	<div class="page_content_wrapper">
		<!-- main column -->
		<div class="main_col">
			<h2>
<?php
				if(is_category()){
					echo 'Category archives: '; 
					single_cat_title();
				}
				elseif(is_tag()){
					echo 'Tag archives: '; 
					single_tag_title();
				}
				elseif(is_author()){
					the_post();
					echo 'Author archives: ' . get_the_author();
					rewind_posts();
				}
				elseif(is_day()){
					echo 'Daily archives: ' . get_the_date();
				}
				elseif(is_month()){
					echo 'Monthly archives: ' . get_the_date('F Y');
				}
				elseif(is_year()){
					echo 'Yearly archives: ' . get_the_date('Y');
				}
				else{
					echo 'Archives';
				}
?>
			</h2>
<?php 
			if(have_posts()){						
				while(have_posts()){
					the_post();
					get_template_part('content', get_post_format());
				}						
			}
?>
		</div><!-- /main_col -->
<?php
		get_sidebar();
?> 
	</div><!-- /page content wrapper -->
<?php
	get_footer();
?>